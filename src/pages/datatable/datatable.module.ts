import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DatatablePage } from './datatable';

@NgModule({
  declarations: [
    DatatablePage,
  ],
  imports: [
    IonicPageModule.forChild(DatatablePage),
  ],
})
export class DatatablePageModule {}
