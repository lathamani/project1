import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';



import { DatatablePage } from '../datatable/datatable';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  username: string;
  password: string;
  message: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad LoginPage');
  // }

  showAlert(alert1) {
    const alert = this.alertCtrl.create({
      message: alert1,
      buttons: ['Ok']
    });
    alert.present();
   
  }
  submit(){ 
  let name=this.username;
  let pwd=this.password;
    if (name==null || name==""){  
     this.showAlert("Invalid username");
    }
    else if (pwd == null || pwd==""){  
      this.showAlert("Invalid Password"); 
    }
    else if (pwd.length<8){  
      this.showAlert("Password length not less that 8"); 
    }else{
      //this.navCtrl.push( HomePage );
      this.navCtrl.setRoot( DatatablePage );
    }
  }


}
